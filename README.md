
DIY Self-publishing tools is a small collection of command line tools for create self-published material.

SVGLayers2PDF.Py creates multipage .PDF documents from .SVG multilayered documents, 
created with Inkscape, asuming layer per page criteria. 
Just remember to save your .SVG documents with all layers visible before invoke the script.

ImposePDF.Py creates final art with proper page imposition ready for printing by distributing pages
in one or several .PDF files.

Requirements from both scripts:

    pdfunite
    pdftocairo
    convert (imagemagick)
    img2pdf
    inkscape
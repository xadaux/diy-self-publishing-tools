#! /usr/bin/python
# -*- coding: iso-8859-15 -*-
#
#	SVGLayers2PDF 0.1 <SVGfile> <DestPDF>
#	Genera un archivo .PDF a partir de las capas de un archivo .SVG
#	
#	2018, Xadauxdaux
#	xadauxdaux@gmail.com
#
#
#
#	Requisitos:
#	Todas las layers tienen que estar visibles, de lo contrario
#	el script genera páginas en blanco. Molaría buscar una solución a esto...#
#	Probamos de usar los verbs pero la mayoría sólo se pueden utilizar
#	con la interfaz y no funcionan desde línea de comandos.
#
#	Bin depends:
#	
#		pdfunite
#		inkscape

import sys
from subprocess import Popen, PIPE
import os
from os.path import basename


print "SVGLayers2PDF.Py 0.1"
# print "El Script no funciona con .svg's generados por Inkscape 0.92 :__("

try:
	if ( len(sys.argv) < 3 ) :
	

		print "Faltan argumentos" 
		print "uso: SVGLayers2PDF.Py <SVGfile> <DestPDF> [-p (preserve temporary files)]"	
		sys.exit()

except ValueError:

	print "Plofff...algo pasó..."


SVGfile = sys.argv[1]
DestPDF = sys.argv[2]

if len(sys.argv) > 3 :
	
	preserveTag = sys.argv[3]

else:

	preserveTag = ""



	#$ inkscape --query-all D.svg | grep layer | awk -F, '{print $1}'

QueryOut = Popen(["inkscape", "--query-all", SVGfile ], stdout=PIPE )
GrepOut = Popen (["grep", 'layer'], stdin=QueryOut.stdout, stdout=PIPE )

# Awk obtiene columnas de resultados:
AwkOut = Popen(["awk", "-F,", "{print $1}"], stdin=GrepOut.stdout, stdout=PIPE )

#Y aquí metemos  todas las layers disponibles en un tuple:
Avail = AwkOut.communicate()[0].split()
Avail.reverse()	#Con esto evitamos generar un .PDF al revés.

print "Avail layers: " + str(Avail)


NLayers = len(Avail)

print "Exporting " + str(NLayers) + " layers"
print "Puedes preservar los archivos temporales añadiendo el tag -p"
print "No olvides que tienen que estar visibiles todas las layers."

workingDir = "/tmp/" + basename(SVGfile) + "tmp/"
		
Popen(["mkdir", workingDir])
		
Count = 0

pdfUniteCMD = [ "pdfunite" ]

TMPdeletionCmd = [ "rm" ]

while Count < NLayers :

	OutFilePart = workingDir + basename ( SVGfile ) + "__part_" + str(Count) + ".pdf"

	print "Generating part....................." + OutFilePart

#	$ inkscape  D.svg -i "layer4" -A out.pdf

	OutFileCmd = ["inkscape", "-C", SVGfile, "-i", Avail[Count], "-A", OutFilePart ]
	p = Popen(OutFileCmd)
	p.wait()
			
	pdfUniteCMD.append(OutFilePart)

	TMPdeletionCmd.append(OutFilePart)

	Count+=1

pdfUniteCMD.append(DestPDF)

print "\nJoining multiple parts to " + DestPDF
p = Popen(pdfUniteCMD)
p.wait()

if preserveTag == "-p" :

	print "\n"
	print "Preserving temporary files :)"
	print "Success!"
	sys.exit()

else :

	print "Deleting Temporary files..............."
	Popen(TMPdeletionCmd).wait()
	Popen(["rmdir", workingDir]).wait()

	print "\n"
	print "Success!"


